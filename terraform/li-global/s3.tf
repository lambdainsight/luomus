resource "aws_s3_bucket" "li-global-prod-artifacts" {
  bucket        = "li-global-prod-artifacts"
  acl           = "private"
#  storage_class = "ONEZONE_IA"
  region        = "eu-central-1"

  tags = {
    name = "li-global-prod-artifacts"
    env  = "prod"
  }
}
