#
# Variables used by this module internally
#
variable "li-euc1-mgmt-perf-instance-type" {
  default = "c5.4xlarge"
}

variable "li-euc1-mgmt-perf-ami" {
  //default = "ami-dd3c0f36" CentOS 7 18.05
  default = "ami-034fffcc6a0063961" // Amazon Linux 2 AMI (HVM), SSD Volume Type
}

variable "li-euc1-mgmt-perf-admin-cidr0" {
  default = "78.139.4.50/32"
}

#
# Input (from other modules)
#

variable "li-euc1-vpc-id" {}
variable "li-euc1-public-subnet-1c-id" {}

#
# Output (for other modules)
#

output "li-euc1-mgmt-perf-instance-ids" {
  value = "${aws_instance.li-euc1-mgmt-perf-instance.*.id}"
}

output "li-euc1-mgmt-perf-instance-names" {
  value = "${aws_instance.li-euc1-mgmt-perf-instance.*.public_dns}"
}
