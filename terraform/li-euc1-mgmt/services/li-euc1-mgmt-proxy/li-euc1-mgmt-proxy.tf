#
### LI-EUC1-MGMT-PROXY
#

#
## SECURITY GROUPS
#

resource "aws_security_group" "li-euc1-mgmt-proxy-sg" {
  name        = "li-euc1-mgmt-proxy-sg"
  description = "li-euc1-mgmt-proxy-sg"
  vpc_id      = "${var.li-euc1-vpc-id}"

  tags {
    Name = "li-euc1-mgmt-proxy-sg"
  }
}

resource "aws_security_group_rule" "li-euc1-mgmt-proxy-to-li-euc1-mgmt-proxy-self-ingress" {
  type              = "ingress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  self              = true
  security_group_id = "${aws_security_group.li-euc1-mgmt-proxy-sg.id}"
}

resource "aws_security_group_rule" "admin-cidr-blocks-to-li-euc1-mgmt-proxy-ssh-ingress" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  security_group_id = "${aws_security_group.li-euc1-mgmt-proxy-sg.id}"
  cidr_blocks       = ["${var.li-euc1-mgmt-proxy-admin-cidr0}"]
}

resource "aws_security_group_rule" "admin-cidr-blocks-to-li-euc1-mgmt-proxy-icmp-echo-request-ingress" {
  type              = "ingress"
  from_port         = 8
  to_port           = -1
  protocol          = "icmp"
  security_group_id = "${aws_security_group.li-euc1-mgmt-proxy-sg.id}"
  cidr_blocks       = ["${var.li-euc1-mgmt-proxy-admin-cidr0}"]
}

resource "aws_security_group_rule" "li-euc1-mgmt-proxy-to-internet-egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = "${aws_security_group.li-euc1-mgmt-proxy-sg.id}"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_instance" "li-euc1-mgmt-proxy-instance" {
  ami           = "${var.li-euc1-mgmt-proxy-ami}"
  instance_type = "${var.li-euc1-mgmt-proxy-instance-type}"
  subnet_id     = "${var.li-euc1-public-subnet-1c-id}"
  key_name      = "istvan"
  count         = 0

  root_block_device {
    volume_size           = "64"
    volume_type           = "gp2"
    delete_on_termination = true
  }

  ebs_block_device {
    volume_size           = "64"
    volume_type           = "gp2"
    delete_on_termination = true
    device_name           = "/dev/sdb"
  }

  vpc_security_group_ids = [
    "${aws_security_group.li-euc1-mgmt-proxy-sg.id}",
  ]

  tags {
    Naming           = "InstanceId.ServiceRole.AvailabilityZone.Stage.DomainName"
    ServiceRole      = "proxy"
    AvailabilityZone = "1c"
    Stage            = "mgmt"
    DomainName       = "aws.li"
    Name             = "li-euc1-mgmt-proxy-instance"
  }

  volume_tags {
    Name = "li-euc1-mgmt-proxy-instance"
  }
}
