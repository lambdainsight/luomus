#
# Variables used by this module internally
#
variable "li-euc1-mgmt-proxy-instance-type" {
  default = "t3.micro"
}

variable "li-euc1-mgmt-proxy-ami" {
  default = "ami-dd3c0f36"
}

variable "li-euc1-mgmt-proxy-admin-cidr0" {
  default = "78.139.4.50/32"
}

#
# Input (from other modules)
#

variable "li-euc1-vpc-id" {}
variable "li-euc1-public-subnet-1c-id" {}

#
# Output (for other modules)
#

