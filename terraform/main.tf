######################## GLOBAL #############################
#
## DNS
#

module "dns" {
  source         = "./dns/"
  li-euc1-vpc-id = "${module.li-euc1-vpc.li-euc1-vpc-id}"
}

#
## LI-GLOBAL
#

module "li-global" {
  source = "./li-global/"
}

#
## CDN
#

module "cdn" {
  source = "./cdn/"
}

######################## VPC ################################

#
## LI-EUC1-VPC
#

# There are only 1 VPC per region, there can be multiple (understanding tradeoffs)

module "li-euc1-vpc" {
  source = "./li-euc1-vpc/"
}

#############################################################

######################## MGMT ###############################
#
## MGMT
#

#
# LI-EUC1-MGMT-PROXY
#

module "li-euc1-mgmt-proxy" {
  source                      = "./li-euc1-mgmt/services/li-euc1-mgmt-proxy/"
  li-euc1-vpc-id              = "${module.li-euc1-vpc.li-euc1-vpc-id}"
  li-euc1-public-subnet-1c-id = "${module.li-euc1-vpc.li-euc1-public-subnet-1c-id}"
}

#
# LI-EUC1-MGMT-PERF
#

module "li-euc1-mgmt-perf" {
  source                      = "./li-euc1-mgmt/services/li-euc1-mgmt-perf/"
  li-euc1-vpc-id              = "${module.li-euc1-vpc.li-euc1-vpc-id}"
  li-euc1-public-subnet-1c-id = "${module.li-euc1-vpc.li-euc1-public-subnet-1c-id}"
}

#############################################################


######################## SERVICES ###########################
#


#############################################################

