resource "aws_s3_bucket" "li-logs" {
  bucket = "li-logs"
  acl    = "private"

  tags = {
    Name = "li-logs"
  }
}

resource "aws_cloudfront_distribution" "www-lambdainsight-com-cloudfront-distribution" {
  origin {
    domain_name = "www.lambdainsight.com.s3.amazonaws.com"
    origin_id   = "S3-lambdainsight"
  }

  enabled             = true
  is_ipv6_enabled     = false
  comment             = "www.lambdainsight.com"
  default_root_object = "index.html"

  logging_config {
    include_cookies = true
    bucket          = "li-logs.s3.amazonaws.com"
    prefix          = "www-lambdainsight-com"
  }

  aliases = ["www.lambdainsight.com", "lambdainsight.com"]

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "S3-lambdainsight"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    compress               = true

    min_ttl     = 86400
    default_ttl = 86400
    max_ttl     = 604800
  }

  price_class = "PriceClass_200"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    Environment = "production"
  }

  viewer_certificate {
    acm_certificate_arn      = "arn:aws:acm:us-east-1:651831719661:certificate/2de91880-2a47-4767-83bc-72b2ad0e9f02"
    minimum_protocol_version = "TLSv1.1_2016"
    ssl_support_method       = "sni-only"
  }
}
