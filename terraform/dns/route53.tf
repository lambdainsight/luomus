resource "aws_route53_zone" "li-prod-public-dns-apex-domain-forward-zone" {
  name    = "lambdainsight.com."
  comment = "li-prod-public-root-route53-zone"

  tags {
    Name = "li-prod-public-route53-zone"
    Env  = "prod"
  }
}

resource "aws_route53_record" "li-prod-public-soa-record" {
  zone_id = "${aws_route53_zone.li-prod-public-dns-apex-domain-forward-zone.zone_id}"
  name    = "lambdainsight.com."
  type    = "SOA"
  ttl     = "3600"

  records = ["ns-1658.awsdns-15.co.uk. awsdns-hostmaster.amazon.com. ${var.li-prod-public-dns-apex-domain-forward-zone-serial-number} 7200 900 1209600 86400"]
}

resource "aws_route53_record" "li-prod-public-ns-record" {
  zone_id = "${aws_route53_zone.li-prod-public-dns-apex-domain-forward-zone.zone_id}"
  name    = "lambdainsight.com."
  type    = "NS"
  ttl     = "172800"

  records = [
    //"ns-311.awsdns-38.com.",
    //"ns-594.awsdns-10.net.",
    //"ns-1389.awsdns-45.org.",
    //"ns-1739.awsdns-25.co.uk.",
    "merlin.ns.cloudflare.com",

    "tegan.ns.cloudflare.com",
  ]
}

resource "aws_route53_record" "li-prod-public-mx-record" {
  zone_id = "${aws_route53_zone.li-prod-public-dns-apex-domain-forward-zone.zone_id}"
  name    = "lambdainsight.com."
  type    = "MX"
  ttl     = "3600"

  records = [
    "1 ASPMX.L.GOOGLE.COM",
    "5 ALT1.ASPMX.L.GOOGLE.COM",
    "5 ALT2.ASPMX.L.GOOGLE.COM",
    "10 ASPMX2.GOOGLEMAIL.COM",
    "10 ASPMX3.GOOGLEMAIL.COM",
  ]
}

resource "aws_route53_record" "li-prod-public-txt-record" {
  zone_id = "${aws_route53_zone.li-prod-public-dns-apex-domain-forward-zone.zone_id}"
  name    = "lambdainsight.com."
  type    = "TXT"
  ttl     = "3600"

  records = ["v=spf1 include:_spf.google.com ~all"]
}

resource "aws_route53_record" "li-prod-public-cname-record-apex" {
  zone_id = "${aws_route53_zone.li-prod-public-dns-apex-domain-forward-zone.zone_id}"
  name    = "_edc14ec5c6c1aa6232a9a8acbd8d9d7c"
  type    = "CNAME"
  ttl     = "3600"

  records = ["_3c7b5a9aa307052e711e37f33681f2dd.acm-validations.aws."]
}

resource "aws_route53_record" "li-prod-public-cname-record-www" {
  zone_id = "${aws_route53_zone.li-prod-public-dns-apex-domain-forward-zone.zone_id}"
  name    = "_0c4c7d137f0ad14045573faf0c78b84f.www"
  type    = "CNAME"
  ttl     = "3600"

  records = ["_8fcfeeafcab5222f6d746363acd4795e.acm-validations.aws."]
}

resource "aws_route53_record" "li-prod-public-a-record" {
  zone_id = "${aws_route53_zone.li-prod-public-dns-apex-domain-forward-zone.zone_id}"
  name    = "lambdainsight.com"
  type    = "A"

  alias {
    name                   = "d29511ek1sifqv.cloudfront.net."
    zone_id                = "Z2FDTNDATAQYW2"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "li-prod-public-www-a-record" {
  zone_id = "${aws_route53_zone.li-prod-public-dns-apex-domain-forward-zone.zone_id}"
  name    = "www.lambdainsight.com"
  type    = "A"

  alias {
    name                   = "d29511ek1sifqv.cloudfront.net."
    zone_id                = "Z2FDTNDATAQYW2"
    evaluate_target_health = false
  }
}

// aws.li
// this is used to name hosts inside the VPC

resource "aws_route53_zone" "li-prod-private-dns-apex-domain-forward-zone" {
  vpc_id  = "${var.li-euc1-vpc-id}"
  name    = "aws.li."
  comment = "li-prod-private-dns-apex-domain-forward-zone"

  tags {
    Name = "li-prod-private-dns-apex-domain-forward-zone"
    Env  = "prod"
  }
}

resource "aws_route53_record" "li-prod-private-dns-apex-domain-forward-zone-soa-record" {
  zone_id = "${aws_route53_zone.li-prod-private-dns-apex-domain-forward-zone.zone_id}"
  name    = "aws.li."
  type    = "SOA"
  ttl     = "3600"

  records = ["ns-1536.awsdns-00.co.uk. awsdns-hostmaster.amazon.com. ${var.li-prod-private-dns-apex-domain-forward-zone-serial-number} 7200 900 1209600 86400"]
}

resource "aws_route53_record" "li-prod-private-dns-apex-domain-forward-zone-ns-record" {
  zone_id = "${aws_route53_zone.li-prod-private-dns-apex-domain-forward-zone.zone_id}"
  name    = "aws.li."
  type    = "NS"
  ttl     = "172800"

  records = [
    "ns-1536.awsdns-00.co.uk.",
    "ns-0.awsdns-00.com.",
    "ns-1024.awsdns-00.org.",
    "ns-512.awsdns-00.net.",
  ]
}

resource "aws_route53_zone" "li-prod-private-dns-apex-domain-reverse-zone" {
  name          = "172.172.in-addr.arpa"
  vpc_id        = "${var.li-euc1-vpc-id}"
  comment       = "li-prod-private-dns-apex-domain-reverse-zone"
  force_destroy = "true"
}

resource "aws_route53_record" "li-prod-private-dns-apex-domain-reverse-zone-soa-record" {
  zone_id = "${aws_route53_zone.li-prod-private-dns-apex-domain-reverse-zone.zone_id}"
  name    = "172.172.in-addr.arpa."
  type    = "SOA"
  ttl     = "3600"

  records = ["ns-1536.awsdns-00.co.uk. awsdns-hostmaster.amazon.com. ${var.li-prod-private-dns-apex-domain-forward-zone-serial-number} 7200 900 1209600 86400"]
}

resource "aws_route53_record" "li-prod-private-dns-apex-domain-reverse-zone-ns-record" {
  zone_id = "${aws_route53_zone.li-prod-private-dns-apex-domain-reverse-zone.zone_id}"
  name    = "172.172.in-addr.arpa."
  type    = "NS"
  ttl     = "172800"

  records = [
    "ns-1536.awsdns-00.co.uk.",
    "ns-0.awsdns-00.com.",
    "ns-1024.awsdns-00.org.",
    "ns-512.awsdns-00.net.",
  ]
}
