#
# Variables used by this module internally
#

variable "li-prod-public-dns-apex-domain-forward-zone-serial-number" {
  default = "1539549558"
}

variable "li-prod-public-aws-subdomain-forward-zone-serial-number" {
  default = "1539805997"
}

variable "li-prod-private-dns-apex-domain-forward-zone-serial-number" {
  default = "1539549558"
}

#
# Input (from other modules)
#
variable "li-euc1-vpc-id" {}

#
# Output (for other modules)
#

output "li-prod-public-dns-apex-domain-forward-zone-id" {
  value = "${aws_route53_zone.li-prod-public-dns-apex-domain-forward-zone.zone_id}"
}

output "li-prod-private-dns-apex-domain-forward-zone-id" {
  value = "${aws_route53_zone.li-prod-private-dns-apex-domain-forward-zone.zone_id}"
}
