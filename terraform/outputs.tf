## DNS

output li-prod-public-dns-apex-domain-forward-zone-id {
  value = "lambdainsight.com zone-id : ${module.dns.li-prod-public-dns-apex-domain-forward-zone-id}"
}

output li-prod-private-dns-apex-domain-forward-zone-id {
  value = "aws.li zone-id : ${module.dns.li-prod-private-dns-apex-domain-forward-zone-id}"
}

## LI-EUC1-VPC

## LI-EUC1-MGMT-PROXY

## LI-EUC1-MGMT-PERF

output li-euc1-mgmt-perf-instance-ids {
  value = ["${module.li-euc1-mgmt-perf.li-euc1-mgmt-perf-instance-ids}"]
}

output li-euc1-mgmt-perf-instance-names {
  value = ["${module.li-euc1-mgmt-perf.li-euc1-mgmt-perf-instance-names}"]
}
