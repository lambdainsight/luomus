# Getting started

```bash
$ terraform get
```

```bash
terraform output -json | jq '.. | .value? // empty'

terraform get -update

terraform init -update
```
