variable "region" {
  default = "eu-central-1"
}

variable "instance-type" {
  default = "t2.nano"
}

variable "amis" {
  default = {
    eu-central-1 = "ami-dd3c0f36"
  }
}

variable "key-name" {
  default = "istvan"
}

#
# Input (from other modules)
#

variable "lambda-insight-prod-vpc-id" {}

variable "lambda-insight-prod-public-subnet-1a-id" {}
variable "lambda-insight-prod-public-subnet-1b-id" {}
variable "lambda-insight-prod-public-subnet-1c-id" {}

variable "lambda-insight-prod-private-subnet-1a-id" {}
variable "lambda-insight-prod-private-subnet-1b-id" {}
variable "lambda-insight-prod-private-subnet-1c-id" {}

#
# Output (for other modules)
#

output "prod-hadoop-01-data-node-security-group-id" {
  value = "${aws_security_group.prod-hadoop-01-data-node.id}"
}

output "prod-hadoop-01-name-node-security-group-id" {
  value = "${aws_security_group.prod-hadoop-01-name-node.id}"
}

output "prod-hadoop-01-journal-node-security-group-id" {
  value = "${aws_security_group.prod-hadoop-01-journal-node.id}"
}
