#
### DATANODES
#

#
## SECURITY GROUPS
#

resource "aws_security_group" "prod-hadoop-01-data-node" {
  name        = "prod-hadoop-01-data-node"
  description = "prod-hadoop-01-data-node"
  vpc_id      = "${var.lambda-insight-prod-vpc-id}"

  tags {
    Name = "prod-hadoop-01-data-node"
  }
}

resource "aws_security_group_rule" "prod-hadoop-01-data-node-to-prod-hadoop-01-data-node-self-ingress" {
  type              = "ingress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  self              = true
  security_group_id = "${aws_security_group.prod-hadoop-01-data-node.id}"
}

resource "aws_security_group_rule" "prod-hadoop-01-data-node-to-prod-hadoop-01-name-node-all-ingress" {
  type                     = "ingress"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
  security_group_id        = "${aws_security_group.prod-hadoop-01-data-node.id}"
  source_security_group_id = "${aws_security_group.prod-hadoop-01-name-node.id}"
}

resource "aws_security_group_rule" "prod-hadoop-01-data-node-to-prod-hadoop-01-journal-node-all-ingress" {
  type                     = "ingress"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
  security_group_id        = "${aws_security_group.prod-hadoop-01-data-node.id}"
  source_security_group_id = "${aws_security_group.prod-hadoop-01-journal-node.id}"
}

resource "aws_security_group_rule" "prod-hadoop-01-data-node-to-all-egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = "${aws_security_group.prod-hadoop-01-data-node.id}"
  cidr_blocks       = ["0.0.0.0/0"]
}

#
## EC2 INSTANCES
#

resource "aws_instance" "prod-hadoop-01-data-node-1a" {
  ami           = "${lookup(var.amis, var.region)}"
  instance_type = "${var.instance-type}"
  count         = 1
  subnet_id     = "${var.lambda-insight-prod-public-subnet-1a-id}"
  key_name      = "${var.key-name}"

  vpc_security_group_ids = [
    "${aws_security_group.prod-hadoop-01-data-node.id}",
  ]

  root_block_device {
    volume_size           = "128"
    volume_type           = "gp2"
    delete_on_termination = true
  }

  ebs_block_device {
    volume_size           = "256"
    volume_type           = "gp2"
    delete_on_termination = true
    device_name           = "/dev/sdb"
  }

  tags {
    Name = "prod-hadoop-01-1a-data-node"
  }
}

resource "aws_instance" "prod-hadoop-01-data-node-1b" {
  ami           = "${lookup(var.amis, var.region)}"
  instance_type = "${var.instance-type}"
  count         = 1
  subnet_id     = "${var.lambda-insight-prod-public-subnet-1b-id}"
  key_name      = "${var.key-name}"

  root_block_device {
    volume_size           = "128"
    volume_type           = "gp2"
    delete_on_termination = true
  }

  ebs_block_device {
    volume_size           = "256"
    volume_type           = "gp2"
    delete_on_termination = true
    device_name           = "/dev/sdb"
  }

  vpc_security_group_ids = [
    "${aws_security_group.prod-hadoop-01-data-node.id}",
  ]

  tags {
    Name = "prod-hadoop-01-1b-data-node"
  }
}

resource "aws_instance" "prod-hadoop-01-data-node-1c" {
  ami           = "${lookup(var.amis, var.region)}"
  instance_type = "${var.instance-type}"
  count         = 1
  subnet_id     = "${var.lambda-insight-prod-public-subnet-1c-id}"
  key_name      = "${var.key-name}"

  root_block_device {
    volume_size           = "128"
    volume_type           = "gp2"
    delete_on_termination = true
  }

  ebs_block_device {
    volume_size           = "256"
    volume_type           = "gp2"
    delete_on_termination = true
    device_name           = "/dev/sdb"
  }

  vpc_security_group_ids = [
    "${aws_security_group.prod-hadoop-01-data-node.id}",
  ]

  tags {
    Name = "prod-hadoop-01-1c-data-node"
  }
}

#
### SERVICE NODES - NAME NODES
#

#
## SECURITY GROUPS
#

resource "aws_security_group" "prod-hadoop-01-name-node" {
  name        = "prod-hadoop-01-name-node"
  description = "prod-hadoop-01-name-node"
  vpc_id      = "${var.lambda-insight-prod-vpc-id}"

  tags {
    Name = "prod-hadoop-01-name-node"
  }
}

resource "aws_security_group_rule" "prod-hadoop-01-name-node-to-prod-hadoop-01-name-node-self-ingress" {
  type              = "ingress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  self              = true
  security_group_id = "${aws_security_group.prod-hadoop-01-name-node.id}"
}

resource "aws_security_group_rule" "prod-hadoop-01-name-node-to-prod-hadoop-01-data-node-all-ingress" {
  type                     = "ingress"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
  security_group_id        = "${aws_security_group.prod-hadoop-01-name-node.id}"
  source_security_group_id = "${aws_security_group.prod-hadoop-01-data-node.id}"
}

resource "aws_security_group_rule" "prod-hadoop-01-name-node-to-prod-hadoop-01-journal-node-all-ingress" {
  type                     = "ingress"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
  security_group_id        = "${aws_security_group.prod-hadoop-01-name-node.id}"
  source_security_group_id = "${aws_security_group.prod-hadoop-01-journal-node.id}"
}

resource "aws_security_group_rule" "prod-hadoop-01-name-node-to-all-egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = "${aws_security_group.prod-hadoop-01-name-node.id}"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_instance" "prod-hadoop-01-name-node-1a" {
  ami           = "${lookup(var.amis, var.region)}"
  instance_type = "${var.instance-type}"
  subnet_id     = "${var.lambda-insight-prod-public-subnet-1a-id}"
  key_name      = "${var.key-name}"

  root_block_device {
    volume_size           = "128"
    volume_type           = "gp2"
    delete_on_termination = true
  }

  ebs_block_device {
    volume_size           = "256"
    volume_type           = "gp2"
    delete_on_termination = true
    device_name           = "/dev/sdb"
  }

  vpc_security_group_ids = [
    "${aws_security_group.prod-hadoop-01-name-node.id}",
  ]

  tags {
    Name = "prod-hadoop-01-1a-name-node"
  }
}

resource "aws_instance" "prod-hadoop-01-name-node-1b" {
  ami           = "${lookup(var.amis, var.region)}"
  instance_type = "${var.instance-type}"
  subnet_id     = "${var.lambda-insight-prod-public-subnet-1b-id}"
  key_name      = "${var.key-name}"

  root_block_device {
    volume_size           = "128"
    volume_type           = "gp2"
    delete_on_termination = true
  }

  ebs_block_device {
    volume_size           = "256"
    volume_type           = "gp2"
    delete_on_termination = true
    device_name           = "/dev/sdb"
  }

  vpc_security_group_ids = [
    "${aws_security_group.prod-hadoop-01-name-node.id}",
  ]

  tags {
    Name = "prod-hadoop-01-1b-name-node"
  }
}

#
### SERVICE NODES - JOURNAL NODES
#

#
## SECURITY GROUPS
#

resource "aws_security_group" "prod-hadoop-01-journal-node" {
  name        = "prod-hadoop-01-journal-node"
  description = "prod-hadoop-01-journal-node"
  vpc_id      = "${var.lambda-insight-prod-vpc-id}"

  tags {
    Name = "prod-hadoop-01-journal-node"
  }
}

resource "aws_security_group_rule" "prod-hadoop-01-journal-node-to-prod-hadoop-01-journal-node-self-ingress" {
  type              = "ingress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  self              = true
  security_group_id = "${aws_security_group.prod-hadoop-01-journal-node.id}"
}

resource "aws_security_group_rule" "prod-hadoop-01-journal-node-to-prod-hadoop-01-data-node-all-ingress" {
  type                     = "ingress"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
  security_group_id        = "${aws_security_group.prod-hadoop-01-journal-node.id}"
  source_security_group_id = "${aws_security_group.prod-hadoop-01-data-node.id}"
}

resource "aws_security_group_rule" "prod-hadoop-01-journal-node-to-prod-hadoop-01-name-node-all-ingress" {
  type                     = "ingress"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
  security_group_id        = "${aws_security_group.prod-hadoop-01-journal-node.id}"
  source_security_group_id = "${aws_security_group.prod-hadoop-01-name-node.id}"
}

resource "aws_security_group_rule" "prod-hadoop-01-journal-node-to-all-egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = "${aws_security_group.prod-hadoop-01-journal-node.id}"
  cidr_blocks       = ["0.0.0.0/0"]
}

#
## EC2 INSTANCES
#

resource "aws_instance" "prod-hadoop-01-journal-node-1a" {
  ami           = "${lookup(var.amis, var.region)}"
  instance_type = "${var.instance-type}"
  subnet_id     = "${var.lambda-insight-prod-public-subnet-1a-id}"
  key_name      = "${var.key-name}"

  root_block_device {
    volume_size           = "128"
    volume_type           = "gp2"
    delete_on_termination = true
  }

  ebs_block_device {
    volume_size           = "256"
    volume_type           = "gp2"
    delete_on_termination = true
    device_name           = "/dev/sdb"
  }

  vpc_security_group_ids = [
    "${aws_security_group.prod-hadoop-01-journal-node.id}",
  ]

  tags {
    Name = "prod-hadoop-01-1a-journal-node"
  }
}

resource "aws_instance" "prod-hadoop-01-journal-node-1b" {
  ami           = "${lookup(var.amis, var.region)}"
  instance_type = "${var.instance-type}"
  subnet_id     = "${var.lambda-insight-prod-public-subnet-1b-id}"
  key_name      = "${var.key-name}"

  root_block_device {
    volume_size           = "128"
    volume_type           = "gp2"
    delete_on_termination = true
  }

  ebs_block_device {
    volume_size           = "256"
    volume_type           = "gp2"
    delete_on_termination = true
    device_name           = "/dev/sdb"
  }

  vpc_security_group_ids = [
    "${aws_security_group.prod-hadoop-01-journal-node.id}",
  ]

  tags {
    Name = "prod-hadoop-01-1b-journal-node"
  }
}

resource "aws_instance" "prod-hadoop-01-journal-node-1c" {
  ami           = "${lookup(var.amis, var.region)}"
  instance_type = "${var.instance-type}"
  subnet_id     = "${var.lambda-insight-prod-public-subnet-1c-id}"
  key_name      = "${var.key-name}"

  root_block_device {
    volume_size           = "128"
    volume_type           = "gp2"
    delete_on_termination = true
  }

  ebs_block_device {
    volume_size           = "256"
    volume_type           = "gp2"
    delete_on_termination = true
    device_name           = "/dev/sdb"
  }

  vpc_security_group_ids = [
    "${aws_security_group.prod-hadoop-01-journal-node.id}",
  ]

  tags {
    Name = "prod-hadoop-01-1c-journal-node"
  }
}
