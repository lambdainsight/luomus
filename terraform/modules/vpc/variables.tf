variable "create-vpc" {
  description = "Controls if VPC should be created (it affects almost all resources)"
  default     = true
}

variable "vpc-cidr" {
  description = "The CIDR block for the VPC. Default value is a valid CIDR, but not acceptable by AWS and should be overridden"
  default     = "0.0.0.0/0"
}

variable "vpc-instance-tenancy" {
  description = "A tenancy option for instances launched into the VPC"
  default     = "default"
}

variable "vpc-enable-dns-hostnames" {
  description = "Should be true to enable DNS hostnames in the VPC"
  default     = true
}

variable "vpc-enable-dns-support" {
  description = "Should be true to enable DNS support in the VPC"
  default     = true
}

variable "vpc-assign-generated-ipv6-cidr-block" {
  description = "ipv6 support for VPC"
  default     = false
}
