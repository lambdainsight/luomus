locals {
  aws-vpc-id = "${aws_vpc.this.id}"
}

resource "aws_vpc_dhcp_options" "this" {
  domain_name         = "${var.aws-vpc-dhcp-options-domain-name}"
  domain_name_servers = ["AmazonProvidedDNS"]

  tags {
    name = "${var.aws_vpc_dhcp_options-name}"
  }
}

resource "aws_vpc" "this" {
  cidr_block                       = "${var.aws-vpc-cidr-block}"
  instance_tenancy                 = "${var.aws-vpc-instance-tenancy}"
  enable_dns_hostnames             = "${var.aws-vpc-enable-dns-hostnames}"
  enable_dns_support               = "${var.aws-vpc-enable-dns-support}"
  assign_generated_ipv6_cidr_block = "${var.aws-vpc-assign-generated-ipv6-cidr-block}"

  tags {
    name = "${var.aws-vpc-name}"
  }
}

resource "aws_vpc_dhcp_options_association" "this" {
  vpc_id          = "${local.aws-vpc-id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.this.id}"
}

resource "aws_network_acl" "this" {
  vpc_id = "${local.aws-vpc-id}"

  subnet_ids = [
    "${aws_subnet.public-subnet-1a.id}",
    "${aws_subnet.public-subnet-1b.id}",
    "${aws_subnet.public-subnet-1c.id}",
    "${aws_subnet.private-subnet-1a.id}",
    "${aws_subnet.private-subnet-1b.id}",
    "${aws_subnet.private-subnet-1c.id}",
  ]

  egress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags {
    Name = "network-acl"
  }
}

resource "aws_subnet" "public-1a" {
  vpc_id                  = "${local.aws-vpc-id}"
  cidr_block              = "${var.aws-subnet-public-1a-cidr-block}"
  availability_zone       = "${var.aws-subnet-public-1a-availability-zone}"
  map_public_ip_on_launch = true

  tags {
    name = "aws-subnet-public-1a"
  }
}

resource "aws_subnet" "public-1b" {
  vpc_id                  = "${local.aws-vpc-id}"
  cidr_block              = "${var.aws-subnet-public-1b-cidr-block}"
  availability_zone       = "${var.aws-subnet-public-1b-availability-zone}"
  map_public_ip_on_launch = true

  tags {
    name = "aws-subnet-public-1b"
  }
}

resource "aws_subnet" "public-1c" {
  vpc_id                  = "${local.aws-vpc-id}"
  cidr_block              = "${var.aws-subnet-public-1c-cidr-block}"
  availability_zone       = "${var.aws-subnet-public-1c-availability-zone}"
  map_public_ip_on_launch = true

  tags {
    name = "aws-subnet-public-1c"
  }
}

resource "aws_subnet" "private-1a" {
  vpc_id                   = "${local.aws-vpc-id}"
  cidr_block               = "${var.aws-subnet-private-1a-cidr-block}"
  availability_zone        = "${var.aws-subnet-public-1a-availability-zone}"
  map_private_ip_on_launch = false

  tags {
    name = "aws-subnet-public-1a}"
  }
}

resource "aws_subnet" "private-1b" {
  vpc_id                   = "${local.aws-vpc-id}"
  cidr_block               = "${var.aws-subnet-private-1b-cidr-block}"
  availability_zone        = "${var.aws-subnet-public-1b-availability-zone}"
  map_private_ip_on_launch = false

  tags {
    name = "aws-subnet-public-1b}"
  }
}

resource "aws_subnet" "private-1c" {
  vpc_id                   = "${local.aws-vpc-id}"
  cidr_block               = "${var.aws-subnet-private-1c-cidr-block}"
  availability_zone        = "${var.aws-subnet-public-1c-availability-zone}"
  map_private_ip_on_launch = false

  tags {
    name = "aws-subnet-public-1c}"
  }
}

resource "aws_internet_gateway" "this" {
  vpc_id = "${local.aws-vpc-id}"

  tags {
    name = "${var.aws-internet-gateway-name}"
  }
}

resource "aws_eip" "this" {
  vpc        = true
  depends_on = ["aws_internet_gateway.this"]

  tags {
    name = "${var.aws-eip-name}"
  }
}

resource "aws_nat_gateway" "this" {
  allocation_id = "${aws_eip.this.id}"
  subnet_id     = "${aws_subnet.this-public-1c.id}"
  depends_on    = ["aws_internet_gateway.this"]

  tags {
    name = "${var.aws-nat-gateway-name}"
  }
}

resource "aws_route_table" "public" {
  vpc_id = "${local.aws-vpc-id}"

  tags {
    name = "${var.aws-route-table-public-name}"
  }
}

resource "aws_route_table" "private" {
  vpc_id = "${local.aws-vpc-id}"

  tags {
    name = "${var.aws-route-table-private-name}"
  }
}

resource "aws_route" "public_internet_gateway" {
  route_table_id         = "${aws_route_table.public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.this.id}"
}

resource "aws_route" "private_internet_gateway" {
  route_table_id         = "${aws_route_table.private.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${aws_nat_gateway.this.id}"
}

resource "aws_main_route_table_association" "this" {
  vpc_id         = "${local.aws-vpc-id}"
  route_table_id = "${aws_route_table.public.id}"
}
