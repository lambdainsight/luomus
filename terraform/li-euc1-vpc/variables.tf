#
# Variables used by this module internally
#

variable "li-euc1-vpc-cidr-block" {
  default = "172.172.0.0/16"
}

variable "li-euc1-public-subnet-1a-cidr-block" {
  default = "172.172.1.0/24"
}

variable "li-euc1-public-subnet-1b-cidr-block" {
  default = "172.172.2.0/24"
}

variable "li-euc1-public-subnet-1c-cidr-block" {
  default = "172.172.3.0/24"
}

variable "li-euc1-private-subnet-1a-cidr-block" {
  default = "172.172.4.0/24"
}

variable "li-euc1-private-subnet-1b-cidr-block" {
  default = "172.172.5.0/24"
}

variable "li-euc1-private-subnet-1c-cidr-block" {
  default = "172.172.6.0/24"
}

variable "li-euc1-vpc-domain-name" {
  default = "aws.li."
}

#
# Input (from other modules)
#

#
# Output (for other modules)
#

output "li-euc1-vpc-id" {
  value = "${aws_vpc.li-euc1-vpc.id}"
}

output "li-euc1-public-subnet-1a-id" {
  value = "${aws_subnet.li-euc1-public-subnet-1a.id}"
}

output "li-euc1-public-subnet-1b-id" {
  value = "${aws_subnet.li-euc1-public-subnet-1b.id}"
}

output "li-euc1-public-subnet-1c-id" {
  value = "${aws_subnet.li-euc1-public-subnet-1c.id}"
}

output "li-euc1-private-subnet-1a-id" {
  value = "${aws_subnet.li-euc1-private-subnet-1a.id}"
}

output "li-euc1-private-subnet-1b-id" {
  value = "${aws_subnet.li-euc1-private-subnet-1b.id}"
}

output "li-euc1-private-subnet-1c-id" {
  value = "${aws_subnet.li-euc1-private-subnet-1c.id}"
}
