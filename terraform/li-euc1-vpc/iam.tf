#
### ROLES & INSTANCE PROFILES
#
#
## LI-EUC1-DEFAULT-ROLE
#

data "aws_iam_policy_document" "li-euc1-default-policy-document" {
  version = "2012-10-17"

  statement {
    sid     = "1"
    actions = ["sts:AssumeRole"]
    effect  = "Allow"

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "li-euc1-default-role" {
  name               = "li-euc1-default-role"
  assume_role_policy = "${data.aws_iam_policy_document.li-euc1-default-policy-document.json}"
}

resource "aws_iam_instance_profile" "li-euc1-default-instance-profile" {
  name = "li-euc1-default-instance-profile"
  role = "${aws_iam_role.li-euc1-default-role.name}"
}

#
### LI-EUC1-DOCKER-ROLE
#
resource "aws_iam_role" "li-euc1-docker-role" {
  name               = "li-euc1-docker-role"
  assume_role_policy = "${data.aws_iam_policy_document.li-euc1-default-policy-document.json}"
}

resource "aws_iam_instance_profile" "li-euc1-docker-instance-profile" {
  name = "li-euc1-docker-instance-profile"
  role = "${aws_iam_role.li-euc1-docker-role.name}"
}

#
### POLICY DOCUMENTS (FOR DEFAULT-ROLE + DOCKER-ROLE)
#
data "aws_iam_policy_document" "li-euc1-allow-describe-ec2-tags-iam-policy-document" {
  version = "2012-10-17"

  statement {
    sid       = "1"
    actions   = ["ec2:DescribeTags"]
    resources = ["*"]
    effect    = "Allow"
  }
}

resource "aws_iam_policy" "li-euc1-allow-describe-ec2-tags-iam-policy" {
  name        = "li-euc1-allow-describe-ec2-tags-iam-policy"
  path        = "/"
  description = "li-euc1-allow-describe-ec2-tags-iam-policy"

  policy = "${data.aws_iam_policy_document.li-euc1-allow-describe-ec2-tags-iam-policy-document.json}"
}

data "aws_iam_policy_document" "li-euc1-allow-change-route53-record-sets-iam-policy-document" {
  version = "2012-10-17"

  statement {
    sid = "1"

    actions = [
      "route53:GetChange",
      "route53:ListHostedZones",
      "route53:ChangeResourceRecordSets",
      "route53:ListHostedZonesByName",
    ]

    resources = ["*"]
    effect    = "Allow"
  }
}

resource "aws_iam_policy" "li-euc1-allow-change-route53-record-sets-iam-policy" {
  name        = "li-euc1-allow-change-route53-record-sets-iam-policy"
  path        = "/"
  description = "li-euc1-allow-change-route53-record-sets-iam-policy"

  policy = "${data.aws_iam_policy_document.li-euc1-allow-change-route53-record-sets-iam-policy-document.json}"
}

// this is wrong, the correct path has to set and we need to split access to read and write cases
// the s3 api is not really user friendly and there must be extra work to allow a certain folder to be
// writable - more on it later 
data "aws_iam_policy_document" "li-euc1-allow-full-access-s3-iam-policy-document" {
  version = "2012-10-17"

  statement {
    sid       = "1"
    actions   = ["s3:*"]
    resources = ["*"]
    effect    = "Allow"
  }
}

resource "aws_iam_policy" "li-euc1-allow-full-access-s3-iam-policy" {
  name        = "li-euc1-allow-full-access-s3-iam-policy"
  path        = "/"
  description = "li-euc1-allow-full-access-s3-iam-policy"

  policy = "${data.aws_iam_policy_document.li-euc1-allow-full-access-s3-iam-policy-document.json}"
}

data "aws_iam_policy_document" "li-euc1-allow-access-ecr-iam-policy-document" {
  version = "2012-10-17"

  statement {
    sid = "1"

    actions = [
      "ecr:CreateRepository",
      "ecr:GetAuthorizationToken",
      "ecr:BatchCheckLayerAvailability",
      "ecr:GetDownloadUrlForLayer",
      "ecr:GetRepositoryPolicy",
      "ecr:DescribeRepositories",
      "ecr:ListImages",
      "ecr:DescribeImages",
      "ecr:BatchGetImage",
      "ecr:InitiateLayerUpload",
      "ecr:UploadLayerPart",
      "ecr:CompleteLayerUpload",
      "ecr:PutImage",
    ]

    resources = ["*"]
    effect    = "Allow"
  }
}

resource "aws_iam_policy" "li-euc1-allow-access-ecr-iam-policy" {
  name        = "li-euc1-allow-access-ecr-iam-policy"
  path        = "/"
  description = "li-euc1-allow-access-ecr-iam-policy"

  policy = "${data.aws_iam_policy_document.li-euc1-allow-access-ecr-iam-policy-document.json}"
}

#
## LI-EUC1-DEFAULT-POLICY-ATTACHMENTS
#
resource "aws_iam_role_policy_attachment" "li-euc1-default-role-and-li-euc1-allow-describe-ec2-tags-iam-policy" {
  role       = "${aws_iam_role.li-euc1-default-role.name}"
  policy_arn = "${aws_iam_policy.li-euc1-allow-describe-ec2-tags-iam-policy.arn}"
}

resource "aws_iam_role_policy_attachment" "li-euc1-default-role-role-and-li-euc1-allow-change-route53-record-sets-iam-policy" {
  role       = "${aws_iam_role.li-euc1-default-role.name}"
  policy_arn = "${aws_iam_policy.li-euc1-allow-change-route53-record-sets-iam-policy.arn}"
}

#
## LI-EUC1-DOCKER-POLICY-ATTACHMENTS
#
resource "aws_iam_role_policy_attachment" "li-euc1-docker-role-and-li-euc1-allow-describe-ec2-tags-iam-policy" {
  role       = "${aws_iam_role.li-euc1-docker-role.name}"
  policy_arn = "${aws_iam_policy.li-euc1-allow-describe-ec2-tags-iam-policy.arn}"
}

resource "aws_iam_role_policy_attachment" "li-euc1-docker-role-role-and-li-euc1-allow-change-route53-record-sets-iam-policy" {
  role       = "${aws_iam_role.li-euc1-docker-role.name}"
  policy_arn = "${aws_iam_policy.li-euc1-allow-change-route53-record-sets-iam-policy.arn}"
}

resource "aws_iam_role_policy_attachment" "li-euc1-docker-role-and-li-euc1-allow-access-ecr-iam-policy" {
  role       = "${aws_iam_role.li-euc1-docker-role.name}"
  policy_arn = "${aws_iam_policy.li-euc1-allow-access-ecr-iam-policy.arn}"
}
