resource "aws_vpc_dhcp_options" "li-euc1-dhcp-options" {
  domain_name         = "${var.li-euc1-vpc-domain-name}"
  domain_name_servers = ["AmazonProvidedDNS"]

  tags {
    Name = "li-euc1-dhcp-options"
  }
}

resource "aws_vpc" "li-euc1-vpc" {
  cidr_block           = "${var.li-euc1-vpc-cidr-block}"
  enable_dns_hostnames = "true"
  enable_dns_support   = "true"

  tags {
    Name = "li-euc1-vpc"
  }
}

resource "aws_vpc_dhcp_options_association" "li-euc1-dhcp-options-association" {
  vpc_id          = "${aws_vpc.li-euc1-vpc.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.li-euc1-dhcp-options.id}"
}

resource "aws_network_acl" "li-euc1-network-acl" {
  vpc_id = "${aws_vpc.li-euc1-vpc.id}"

  subnet_ids = [
    "${aws_subnet.li-euc1-public-subnet-1a.id}",
    "${aws_subnet.li-euc1-public-subnet-1b.id}",
    "${aws_subnet.li-euc1-public-subnet-1c.id}",
    "${aws_subnet.li-euc1-private-subnet-1a.id}",
    "${aws_subnet.li-euc1-private-subnet-1b.id}",
    "${aws_subnet.li-euc1-private-subnet-1c.id}",
  ]

  egress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags {
    Name = "li-euc1-network-acl"
  }
}

resource "aws_subnet" "li-euc1-public-subnet-1a" {
  vpc_id                  = "${aws_vpc.li-euc1-vpc.id}"
  cidr_block              = "${var.li-euc1-public-subnet-1a-cidr-block}"
  availability_zone       = "eu-central-1a"
  map_public_ip_on_launch = true

  tags {
    Name = "li-euc1-public-subnet-1a"
  }
}

resource "aws_subnet" "li-euc1-public-subnet-1b" {
  vpc_id                  = "${aws_vpc.li-euc1-vpc.id}"
  cidr_block              = "${var.li-euc1-public-subnet-1b-cidr-block}"
  availability_zone       = "eu-central-1b"
  map_public_ip_on_launch = true

  tags {
    Name = "li-euc1-public-subnet-1b"
  }
}

resource "aws_subnet" "li-euc1-public-subnet-1c" {
  vpc_id                  = "${aws_vpc.li-euc1-vpc.id}"
  cidr_block              = "${var.li-euc1-public-subnet-1c-cidr-block}"
  availability_zone       = "eu-central-1c"
  map_public_ip_on_launch = true

  tags {
    Name = "li-euc1-public-subnet-1c"
  }
}

resource "aws_subnet" "li-euc1-private-subnet-1a" {
  vpc_id                  = "${aws_vpc.li-euc1-vpc.id}"
  cidr_block              = "${var.li-euc1-private-subnet-1a-cidr-block}"
  availability_zone       = "eu-central-1a"
  map_public_ip_on_launch = false

  tags {
    Name = "li-euc1-private-subnet-1a"
  }
}

resource "aws_subnet" "li-euc1-private-subnet-1b" {
  vpc_id                  = "${aws_vpc.li-euc1-vpc.id}"
  cidr_block              = "${var.li-euc1-private-subnet-1b-cidr-block}"
  availability_zone       = "eu-central-1b"
  map_public_ip_on_launch = false

  tags {
    Name = "li-euc1-private-subnet-1b"
  }
}

resource "aws_subnet" "li-euc1-private-subnet-1c" {
  vpc_id                  = "${aws_vpc.li-euc1-vpc.id}"
  cidr_block              = "${var.li-euc1-private-subnet-1c-cidr-block}"
  availability_zone       = "eu-central-1c"
  map_public_ip_on_launch = false

  tags {
    Name = "li-euc1-private-subnet-1c"
  }
}

resource "aws_internet_gateway" "li-euc1-internet-gateway" {
  vpc_id = "${aws_vpc.li-euc1-vpc.id}"

  tags {
    Name = "li-euc1-internet-gateway"
  }
}

resource "aws_route_table" "li-euc1-route-table" {
  vpc_id = "${aws_vpc.li-euc1-vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.li-euc1-internet-gateway.id}"
  }

  tags {
    Name = "li-euc1-route-table"
  }
}

// main route tables are implicitly associated with subnets
resource "aws_main_route_table_association" "li-euc1-main-route-table-association" {
  vpc_id         = "${aws_vpc.li-euc1-vpc.id}"
  route_table_id = "${aws_route_table.li-euc1-route-table.id}"
}

resource "aws_route_table_association" "li-euc1-main-route-table-public-subnet-1a-association" {
  subnet_id      = "${aws_subnet.li-euc1-public-subnet-1a.id}"
  route_table_id = "${aws_route_table.li-euc1-route-table.id}"
}

resource "aws_route_table_association" "li-euc1-main-route-table-public-subnet-1b-association" {
  subnet_id      = "${aws_subnet.li-euc1-public-subnet-1b.id}"
  route_table_id = "${aws_route_table.li-euc1-route-table.id}"
}

resource "aws_route_table_association" "li-euc1-main-route-table-public-subnet-1c-association" {
  subnet_id      = "${aws_subnet.li-euc1-public-subnet-1c.id}"
  route_table_id = "${aws_route_table.li-euc1-route-table.id}"
}

resource "aws_route_table_association" "li-euc1-main-route-table-private-subnet-1a-association" {
  subnet_id      = "${aws_subnet.li-euc1-private-subnet-1a.id}"
  route_table_id = "${aws_route_table.li-euc1-route-table.id}"
}

resource "aws_route_table_association" "li-euc1-main-route-table-private-subnet-1b-association" {
  subnet_id      = "${aws_subnet.li-euc1-private-subnet-1b.id}"
  route_table_id = "${aws_route_table.li-euc1-route-table.id}"
}

resource "aws_route_table_association" "li-euc1-main-route-table-private-subnet-1c-association" {
  subnet_id      = "${aws_subnet.li-euc1-private-subnet-1c.id}"
  route_table_id = "${aws_route_table.li-euc1-route-table.id}"
}

// This clears all rules from the default acl
resource "aws_default_network_acl" "li-euc1-default-network-acl" {
  default_network_acl_id = "${aws_vpc.li-euc1-vpc.default_network_acl_id}"

  tags {
    Name = "li-euc1-default-network-acl"
  }
}

// terraform import module.global.aws_default_security_group.default-sg sg-09d44b3d9a329f378
// this has to be done after the first run of creating a VPC when the default sg id is known
// tis clears all rules from it
resource "aws_default_security_group" "default-sg" {
  vpc_id = "${aws_vpc.li-euc1-vpc.id}"

  tags {
    Name = "default-sg"
  }
}

resource "aws_vpc_endpoint" "li-euc1-s3-endpoint" {
  vpc_id       = "${aws_vpc.li-euc1-vpc.id}"
  service_name = "com.amazonaws.eu-central-1.s3"

  route_table_ids = [
    "${aws_route_table.li-euc1-route-table.id}",
  ]

  #   policy = <<EOP
  # {
  #     "Statement": [
  #         {
  #             "Action": "*",
  #             "Effect": "Allow",
  #             "Resource": "*",
  #             "Principal": "*"
  #         }
  #     ]
  # }
  # EOP
}
