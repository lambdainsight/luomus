#!/usr/bin/env python

import sys, httplib, urllib2, json, logging, yaml, os, jinja2, boto3

#
#   tags {
#       Naming           = "InstanceId.ServiceRole.AvailabilityZone.Stage.DomainName"
#
#       ServiceRole      = "proxy"
#       AvailabilityZone = "1c"
#       Stage            = "mgmt"
#       DomainName       = "aws.li"
#
#       Name             = "li-mgmt-proxy-instance"
# }
#

def get_instance_identity_document():
  response = urllib2.urlopen('http://169.254.169.254/latest/dynamic/instance-identity/document')
  if response.getcode() == 200:
    return {'ok': json.loads(response.read())}
  else:
    return {'err': 'err'}

def parse_instance_identity_document(doc):
  instance_id       = doc.get('instanceId', 'i-00000000000000000')
  private_ip        = doc.get('privateIp', '127.127.127.127')
  aws_region        = doc.get('region', 'eu-central-1')
  availability_zone = doc.get('availabilityZone', 'eu-central-1c')
  return (instance_id, private_ip, aws_region, availability_zone)

def get_naming():
  return 0

def provision():
  logging.info('Provisioning starting...')
  logging.info(parse_instance_identity_document(get_instance_identity_document().get('ok', {})))
  logging.info('Provisioning ending...')

def main():
  try:
    logging.basicConfig(stream=sys.stdout,
                        level=logging.INFO,
                        format='%(asctime)s %(levelname)-8s %(message)s',
                        datefmt='%a, %d %b %Y %H:%M:%S')
    logging.info('Interpreter location: %s', sys.executable)
    provision()
  except KeyboardInterrupt:
    logging.info("Ctrl+c was pressed, exiting...")
    sys.exit()
if __name__ == '__main__':
  main()
