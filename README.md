# Luomus

Creating a DWH in the cloud

## Goals

There are two aspects of creating an DWH: allocating capacity, deploying & configuring the required services.

(This is AWS specific, todo: GCP,...)

- ability to create capacity in AWS with the following services:
 - VPC
 - S3
 - EMR
 - Athena
 - QuickSight
 - EC2

- ability to configure and deploy operating system and application changes

## Naming Convention

### Terraform Resources

- module

module "li-euc1-vpc" {}

- resource

resource "aws_instance" "li-euc1-mgmt-proxy-instance" { }

### Host Names

- {instance_id}.{role_type}.{stage}.{cluster_id}.{az}.{vpc_id|NULL}.{region_id|NULL}.{domain}.{tld}
  - Example: i-06d3c2580b1e1651d.data-node.dev.online.1a.vpc-853663ee.euc1.aws.li.
  - Example: i-06d3c2580b1e1651d.data-node.dev.online.1a.euc1.aws.li.
  - Example: i-06d3c2580b1e1651d.data-node.dev.online.1a.aws.li.

## Tech

There are few tools that can solve the problems above.

Current tools:

[Terraform](https://www.terraform.io/)

[Ansible](https://www.ansible.com/)

## License

See LICENSE & NOTICE files

Copyright 2018 Lambda Insight

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
